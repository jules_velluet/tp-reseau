#!/usr/bin/python3
import socket
from threading import Thread

class Server:

    def __init__(self, ip, port):
        self.COUNTER = 0
        self.ip = ip
        self.port = port

    def start (self):
        sock = socket.socket()
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((self.ip, self.port))
        sock.listen(10)
        while True:
            cli, addr = sock.accept()
            self.COUNTER += 1
            t = Thread(target=Session(cli, self).start)
            t.start()


class Session:

    def __init__(self, cli, server):
        self.server = server
        self.cli = cli

    def start(self):
        f = self.cli.makefile(mode="rw")
        while True:
            cmd = f.readline().strip()
            if cmd == "get":
                f.write("val %d\n" % self.server.COUNTER)
                f.flush()
            elif cmd == "quit":
                f.write("quit\n")
                f.flush()
                break
            else:
                f.write("err\n")
                f.flush()
        f.close()
        self.cli.shutdown(socket.SHUT_RDWR)
        self.cli.close()
        self.server.COUNTER -= 1

server = Server("0.0.0.0", 4444)
server.start()

#! /usr/bin/python3
import socket

class server():
    def __init__(self,port):
        self.COUNTER = 0
        sock = socket.socket()
        sock.setsockopt(socket.SOL_SOCKET , socket.SO_REUSEADDR , 1)
        sock.bind(("0.0.0.0", port))
        sock.listen(10)
        while True:
            cli, addr = sock.accept()
            self.COUNTER += 1
            session(cli,self.COUNTER)

class session():
    def __init__(self,cli,COUNTER):
        f = cli.makefile(mode="rw")
        while True:
            cmd = f.readline().strip()
            if cmd == "get":
                f.write("val %d\n" % COUNTER)
                f.flush()
            elif cmd == "quit":
                f.write("quit\n")
                f.flush()
                break
            else:
                f.write("err\n")
                f.flush()
        f.close()
        cli.shutdown(socket.SHUT_RDWR)
        cli.close()
        self.server.COUNTER -= 1

server(4444)

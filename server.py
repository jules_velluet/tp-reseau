import socket

#construction de la socket de type UDP
ma_socket = socket.socket(type= socket.SOCK_DGRAM )

#liaison de la socket a l'adresse ip et au port specifie
ma_socket.bind (("127.0.0.1",5555))

#lecture de donee
BUFSIZE=1024
data , address = ma_socket.recvfrom(BUFSIZE)
print(data.decode())

#envoie de donnee
ma_socket.sendto(b"Bien recu", address)

import socket
import datetime
import time

def server(port):
    sock = socket.socket(type=socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)
    addr = ("255.255.255.255", port)
    while True:
        data = str("Velluet: ")+input()
        sock.sendto(data.encode(), addr)
        time.sleep(1.0)

server(6666)
